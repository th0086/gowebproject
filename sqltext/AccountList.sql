CREATE TABLE `AccountList` (
                               `Account` varchar(20) NOT NULL DEFAULT '' COMMENT '姓名',
                               `Password` varchar(10) DEFAULT NULL COMMENT '密碼',
                               `Tel` varchar(20) DEFAULT NULL COMMENT '電話',
                               `Email` varchar(50) DEFAULT NULL COMMENT '電郵',
                               `CreateDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT '建立时间',
                               PRIMARY KEY (`Account`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;