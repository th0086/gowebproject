CREATE TABLE `account`.`Sys_Log` (
                                     `Id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                                     `HttpStatus` smallint(5) unsigned NOT NULL,
                                     `HttpMethod` varchar(10) NOT NULL COMMENT '請求方式',
                                     `ClientIp` varchar(50) NOT NULL COMMENT '請求端位址',
                                     `Request` varchar(100) NOT NULL COMMENT '請求目標',
                                     `Parameter` text NOT NULL COMMENT '請求參數',
                                     `Log` varchar(100) NOT NULL COMMENT '請求的結果',
                                     `ResponseMessage` text,
                                     `CreateUser` varchar(50) NOT NULL COMMENT '合法的請求者',
                                     `CreateDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '建立時間',
                                     PRIMARY KEY (`Id`) USING BTREE,
                                     KEY `idx_date_level` (`CreateDate`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE utf8mb4_general_ci;