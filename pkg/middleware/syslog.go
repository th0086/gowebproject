package middleware

import (
	"bytes"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"gopractise/pkg/syslogengine"
	"io/ioutil"
)

type responseBodyWriter struct {
	gin.ResponseWriter
	body   *bytes.Buffer
	status int
}

func (r *responseBodyWriter) WriteHeader(status int) {
	r.status = status
	r.ResponseWriter.WriteHeader(status)
}
func (r responseBodyWriter) Write(b []byte) (int, error) {
	r.body.Write(b)
	return r.ResponseWriter.Write(b)
}

func SysLog(sysLog *syslogengine.SysLogAdapter) gin.HandlerFunc {
	return func(ctx *gin.Context) {

		bodyBytes, _ := ioutil.ReadAll(ctx.Request.Body)
		ctx.Request.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

		w := &responseBodyWriter{body: &bytes.Buffer{}, ResponseWriter: ctx.Writer}
		ctx.Writer = w
		ctx.Next()

		logErr := sysLog.LogProcessAdapter(w.status, ctx.Request.URL.Path, ctx.Request.Method, string(bodyBytes), w.body.String(), ctx)
		if logErr != nil {
			log.Err(logErr).Msgf("LogProcessAdapter failed ")
		}

	}
}