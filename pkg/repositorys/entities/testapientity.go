package entities

type AccountInfo struct {
	Account    string    `json:"account"`
	Tel        int       `json:"tel"`
	Email      string    `json:"email"`
}

type LogDetailIntoDB struct {
	Level           string
	ClientIp        string
	HttpStatus      int
	HttpMethod      string
	Request         string
	Parameter       string
	Log             string
	ResponseMessage string
	CreateUser      string
	CreateDate      string
}