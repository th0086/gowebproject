package repositorys

import (
	"database/sql"
	"github.com/rs/zerolog/log"
	"gopractise/pkg/repositorys/entities"
	"gopractise/pkg/services/contract"
)

type TestApiRepository struct {
	dbConnections *sql.DB
}

func NewTestApiRepository(db *sql.DB) *TestApiRepository {
	return &TestApiRepository{
		dbConnections: db,
	}
}

func (t *TestApiRepository) QueryAccountInfo(query contract.GetAccountInfoQuery) (*entities.AccountInfo, error) {

	queryAccount := query.Account
	queryPassWord := query.PassWord

	sqlStr := " SELECT Account,Tel,Email FROM account.accountList "
	sqlWhere := " WHERE Account = '" + queryAccount + "' AND Password = '" + queryPassWord + "';"

	res, err := t.dbConnections.Query(sqlStr+sqlWhere)
	if err != nil {
		log.Err(err).Msgf("Failed mysql connection")
		return nil, err
	}
	defer func(res *sql.Rows) {
		err := res.Close()
		if err != nil {
			log.Err(err).Msgf("Failed mysql row")
		}
	}(res)

	var account string
	var tel int
	var email string
	for res.Next() {
		err :=  res.Scan(&account, &tel, &email)
		if err != nil {
			log.Err(err).Msgf("Failed mysql parse row")
			return nil, err
		}
	}

	accountInfoListResult := entities.AccountInfo{
		Account: account,
		Tel: tel,
		Email: email,
	}

	return &accountInfoListResult, nil
}

func (t *TestApiRepository) InsertLog(data entities.LogDetailIntoDB) error {
	sqlStr := `INSERT INTO account.sys_Log (ClientIp,HttpStatus,HttpMethod,Request,Parameter,Log,ResponseMessage,CreateUser,CreateDate) VALUES (?,?,?,?,?,?,?,?,?)`

	_, err := t.dbConnections.Exec(sqlStr, data.ClientIp, data.HttpStatus, data.HttpMethod, data.Request, data.Parameter, data.Log, data.ResponseMessage, data.CreateUser, data.CreateDate)
	if err != nil {
		log.Err(err).Msgf("Failed mysql connection")
		return err
	}
	return nil
}