package serviceengine

import (
	"database/sql"
	"gopractise/pkg/repositorys"

	httpHandler "gopractise/pkg/delivery/http"
	"gopractise/pkg/services"

	"github.com/gin-gonic/gin"
)

type TestApi struct {
}


func (t *TestApi) AddService(engine *gin.RouterGroup, mysql *sql.DB) {
	TestApiRepository := repositorys.NewTestApiRepository(mysql)

	TestApiService := services.NewTestApiService(TestApiRepository)
	TestApiHTTPHandler := httpHandler.NewTestApiHTTPHandler(TestApiService)

	engine.GET("/testApi", TestApiHTTPHandler.GetAccountInfo)
}