package serviceengine

import (
	"database/sql"

	"github.com/gin-gonic/gin"
)

type ModuleAdd interface {
	AddService(engine *gin.RouterGroup, mysql *sql.DB)
}