package services

import (
	"gopractise/pkg/repositorys"
	"gopractise/pkg/repositorys/entities"
	"gopractise/pkg/services/contract"
)

type TestApiService struct {
	TestApiRepository *repositorys.TestApiRepository
}

func NewTestApiService(TestApiRepository *repositorys.TestApiRepository) *TestApiService {
	return &TestApiService{
		TestApiRepository,
	}
}

func (t *TestApiService) GetAccountList(
	query contract.GetAccountInfoQuery) (*entities.AccountInfo, error) {

	result, err := t.TestApiRepository.QueryAccountInfo(query)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func (t *TestApiService) InsertSysLog(data entities.LogDetailIntoDB) error {
	err := t.TestApiRepository.InsertLog(data)
	if err != nil {
		return err
	}
	return nil
}