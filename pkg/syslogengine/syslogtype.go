package syslogengine

import "github.com/gin-gonic/gin"

type SysLogType interface {
	ProcessAuditLog(statusCode int, requestBody string, response string, ctx *gin.Context) (string, string, error)
}