package syslogengine

import (
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"gopractise/pkg/repositorys/entities"
	"gopractise/pkg/services"
	"time"
)

type SysLogAdapter struct {
	sysLogs       map[string]map[string]SysLogType
	testApiService *services.TestApiService
}

// status- message
var responseStatusMap = map[int][]string{
	200: {"成功。", ""},
	204: {"成功。", ""},
	400: {"失敗。", "錯誤訊息: 資料格式錯誤。"},
	500: {"失敗。", "錯誤訊息: 系统内部錯誤。"},
}

func NewSysLogAdapter(sysLogs map[string]map[string]SysLogType, testApiService *services.TestApiService) *SysLogAdapter {
	return &SysLogAdapter{
		sysLogs:        sysLogs,
		testApiService: testApiService,
	}
}

func (s *SysLogAdapter) LogProcessAdapter(statusCode int, router string, httpEvent, requestBody string, response string, ctx *gin.Context) error {
	instance, ok := s.sysLogs[router][httpEvent]
	if ok {
		actionLog, responseBody, err := instance.ProcessAuditLog(statusCode, requestBody, response, ctx)
		if err != nil {
			log.Err(err).Msgf("ProcessAuditLog failed")
			return err
		}
		timezone, _ := time.LoadLocation("Asia/Shanghai") //set to +8 timezone

		data := entities.LogDetailIntoDB{
			ClientIp:        getRealIP(ctx),
			HttpStatus:      statusCode,
			HttpMethod:      httpEvent,
			Request:         router,
			Parameter:       requestBody,
			Log:             actionLog,
			ResponseMessage: responseBody,
			CreateUser:      "test",
			CreateDate:      time.Now().In(timezone).Format("2006-01-02 15:04:05"),
		}

		insertErr := s.testApiService.InsertSysLog(data)
		if insertErr != nil {
			log.Err(insertErr).Msgf("InsertAudioLog failed ")
			return insertErr
		}
		return nil
	}
	return nil
}

func getRealIP(ctx *gin.Context) string {
	r := ctx.Request
	IPAddress := r.Header.Get("X-Real-IP")
	if IPAddress == "" {
		IPAddress = ctx.ClientIP()
	}
	return IPAddress
}