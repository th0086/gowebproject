package syslogengine

import (
	"fmt"
	"github.com/gin-gonic/gin"
)

type TestApiSelect struct {
}

func NewTestApiSelect() *TestApiSelect {
	return &TestApiSelect{}
}

func (t *TestApiSelect) ProcessAuditLog(statusCode int, requestBody string, responseBody string, ctx *gin.Context) (string, string, error) {


	statusMapStringList, ok := responseStatusMap[statusCode]
	if !ok {
		return "", "", fmt.Errorf("%s", "get statusMap fail!")
	}

	var actionStatus, errorMessage string
	if len(statusMapStringList) == 2 {
		actionStatus = statusMapStringList[0]
		errorMessage = statusMapStringList[1]
	}

	actionLog := "測試搜尋" + actionStatus + errorMessage

	return actionLog, responseBody, nil
}