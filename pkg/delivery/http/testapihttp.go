package http

import (
	"gopractise/pkg/services/contract"
	"net/http"

	"gopractise/pkg/services"

	"github.com/gin-gonic/gin"
)

type TestApiHTTPHandler struct {
	testApiService *services.TestApiService
}

func NewTestApiHTTPHandler(triggerService *services.TestApiService,)*TestApiHTTPHandler {
	return &TestApiHTTPHandler{
		triggerService,
	}
}

func (t *TestApiHTTPHandler) GetAccountInfo(ctx *gin.Context) {
	var query contract.GetAccountInfoQuery
	if err := ctx.ShouldBind(&query); err != nil {
		ctx.String(http.StatusBadRequest, err.Error())
		return
	}
	result, err := t.testApiService.GetAccountList(query)
	if err != nil {
		ctx.String(http.StatusInternalServerError, err.Error())
		return
	}
	ctx.JSON(http.StatusOK, result)
}