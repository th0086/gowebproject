package main

import (
	"database/sql"
	"gopractise/pkg/middleware"
	"gopractise/pkg/repositorys"
	"gopractise/pkg/services"
	"gopractise/pkg/syslogengine"
	"net/http"

	configManager "gopractise/internal/config"

	"gopractise/internal/logformat"
	"gopractise/pkg/serviceengine"

	//"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/go-sql-driver/mysql"
	"github.com/rs/zerolog/log"
)

func initSetting() {
	//init log
	log.Logger = logformat.InitLogger()
	//init config
	configManager.Global = configManager.Reload()
}

func initializeService() (*gin.Engine, *sql.DB, error) {
	//inject api service
	var newModules []serviceengine.ModuleAdd
	testApi := serviceengine.TestApi{}

	newModules = append(newModules, &testApi)

	ginEngine, sqlDB := newGinEngines(newModules)
	return ginEngine, sqlDB, nil
}

func newGinEngines(service []serviceengine.ModuleAdd) (*gin.Engine, *sql.DB) {
 	mysqlDB := ConnectMysqlDB()
	sysLogEngine := createSysLogEngine(mysqlDB)
	appEngine := gin.Default()

	appEngine.Use(middleware.SysLog(sysLogEngine))
	routerGroup := appEngine.Group("/v1")
	for _, instance := range service {
		instance.AddService(routerGroup, mysqlDB)
	}

	appEngine.NoRoute(func(ctx *gin.Context) {
		ctx.String(http.StatusNotFound, "main: this url is not found on this service")
	})
	return appEngine, mysqlDB
}

func ConnectMysqlDB() *sql.DB {
	mysqlConfig := mysql.Config{
		User:                 configManager.Global.ApiGroups.Mysql.Username,
		Passwd:               configManager.Global.ApiGroups.Mysql.Password,
		Addr:                 configManager.Global.ApiGroups.Mysql.Address,
		Net:                  "tcp",
		DBName:               configManager.Global.ApiGroups.Mysql.Database,
		AllowNativePasswords: true,
		MultiStatements:      true,
	}

	db, err := sql.Open("mysql", mysqlConfig.FormatDSN())
	if err != nil {
		log.Panic().Err(err).Msgf("main: open database connection failed")
	}
	if err = db.Ping(); err != nil {
		log.Panic().Err(err).Msgf("main: mysql db connection failed")
	}

	db.SetMaxOpenConns(100)
	db.SetMaxIdleConns(100)
	return db
}

func createSysLogEngine(db *sql.DB) *syslogengine.SysLogAdapter {
	testApiRepository := repositorys.NewTestApiRepository(db)
	testApiService := services.NewTestApiService(testApiRepository)
	sysLogInstances := make(map[string]map[string]syslogengine.SysLogType)

	sysLogInstances["/v1/testApi"] = make(map[string]syslogengine.SysLogType)
	sysLogInstances["/v1/testApi"]["GET"] = syslogengine.NewTestApiSelect()
	sysLog := syslogengine.NewSysLogAdapter(sysLogInstances, testApiService)

	return sysLog
}