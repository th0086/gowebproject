package configmanager

import (
	"io/ioutil"

	"github.com/rs/zerolog/log"
	"gopkg.in/yaml.v2"
)

// Global GlobalConfig 全Service可使用的參數
var Global *Configuration

type Mysql struct {
	Address  string `yaml:"address"`
	Username string `yaml:"username"`
	Password string `yaml:"password"`
	Database string `yaml:"database"`
}

//Configuration 參數結構
type Configuration struct {
	//WSServer jiface.IServer
	Env string `yaml:"env"`

	ApiGroups struct {
		HTTPBind     string    `yaml:"http_bind"`
		Mysql        Mysql     `yaml:"mysql"`
	} `yaml:"api_groups"`
}

//Reload 重新載入參數
func Reload() *Configuration {
	data, err := ioutil.ReadFile("configs/config.yml")
	if err != nil {
		log.Panic().Msgf("%v", err)
	}

	tempPara := &Configuration{}

	err = yaml.Unmarshal(data, &tempPara)
	if err != nil {
		log.Panic().Msgf("%v", err)
	}

	log.Info().Msgf("%v", tempPara)

	return tempPara
}
